package interfaces;

import java.util.ResourceBundle;

import javax.swing.JFrame;

public interface IFrame {
	


		boolean isOpenDialog();

		String getFeatureName();

		void openDialog(ResourceBundle bundle);

		void setPosition();
		
		JFrame getCurrentFrame();
}
