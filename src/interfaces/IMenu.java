package interfaces;

import java.util.ArrayList;
import javax.swing.JFrame;
import loader.UIFeature;

public interface IMenu {
	
	public ArrayList<UIFeature> getFeatureList();
	public void addFeature(UIFeature feature);
	public JFrame getCurrentFrame();
	

}
