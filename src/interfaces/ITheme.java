package interfaces;

import java.awt.Color;
import java.awt.Font;

public interface ITheme {
	
	int size = 14;
	
	static int getThemeSize() {
		return size;
	}
	
	static String getThemeBackground(){
		return "";
	}
	
	static Font getFont() {
		return new Font(Font.SERIF, 1, size);
	}
	
	static Color getColor() {
		return Color.BLACK;
	}



}
