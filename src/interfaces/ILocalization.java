package interfaces;

import java.util.Locale;
import java.util.ResourceBundle;

public interface ILocalization {
	public Locale getLocale();
	public String getCountryName();
	ResourceBundle getBundle();
}
