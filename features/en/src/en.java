import java.util.Locale;
import java.util.ResourceBundle;

import interfaces.ILocalization;

public class en implements ILocalization{

	@Override
	public Locale getLocale() {
		return new Locale("US", "en");
	}

	@Override
	public String getCountryName() {
		return "US";
	}

	@Override
	public ResourceBundle getBundle() {
		// TODO Auto-generated method stub
		return null;
	}

}
