
import java.awt.Font;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.ResourceBundle;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import com.itextpdf.text.Document;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class PDFGenerator {

	Connection conn = null;
	ResultSet rs = null;
	PreparedStatement pst = null;
	login l = new login(null);
	ResourceBundle messages = l.getBundle();

	private void jMenuItemPDFreportCreation(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jMenuItem6ActionPerformed
		// #if ApplicationLogging
//@		 ApplicationLogger.logger.info("Create PDF Report on Employees.");
		// #endif
		JFileChooser dialog = new JFileChooser();
		dialog.setSelectedFile(new File("Employees Report.pdf"));
		int dialogResult = dialog.showSaveDialog(null);
		if (dialogResult == JFileChooser.APPROVE_OPTION) {
			String filePath = dialog.getSelectedFile().getPath();

			try {
				// #if ApplicationLogging
//@				 ApplicationLogger.logger.info("Trying to select all data from TABLE"+
//@				 "STAFF_INFORMATION.");
				// #endif
				String sql = "select * from Staff_information";

				pst = conn.prepareStatement(sql);
				rs = pst.executeQuery();

				Document myDocument = new Document();
				PdfWriter myWriter = PdfWriter.getInstance(myDocument, new FileOutputStream(filePath));
				PdfPTable table = new PdfPTable(13);
				myDocument.open();

				float[] columnWidths = new float[] { 3, 8, 7, 5, 5, 9, 8, 9, 6, 6, 6, 8, 8 };
				table.setWidths(columnWidths);

				table.setWidthPercentage(100); // set table width to 100%

				myDocument.add(new Paragraph(messages.getString("emp_list"),
						FontFactory.getFont(FontFactory.TIMES_BOLD, 20, Font.BOLD)));
				myDocument.add(new Paragraph(new Date().toString()));
				myDocument.add(new Paragraph(
						"-------------------------------------------------------------------------------------------"));
				table.addCell(
						new PdfPCell(new Paragraph("ID", FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("firstname"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("surname"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("date_of_birth"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("email"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("tel"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("address"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("department"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("gender"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("salary"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("status"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("date_hired"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("job_titel"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.BOLD))));

				while (rs.next()) {

					table.addCell(new PdfPCell(new Paragraph(rs.getString(1),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(2),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(3),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(4),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(5),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(6),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(7),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(8),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(10),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(11),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(16),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(17),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(18),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.PLAIN))));

				}

				myDocument.add(table);
				myDocument.add(new Paragraph(
						"--------------------------------------------------------------------------------------------"));
				myDocument.close();
				// #if ApplicationLogging
//@				 ApplicationLogger.logger.info("Document successfully created.");
				// #endif

				JOptionPane.showMessageDialog(null, messages.getString("report_sucessful"));

			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, e);

			} finally {

				try {
					rs.close();
					pst.close();

				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, e);

				}
			}
		}

	}// GEN-LAST:event_jMenuItem6ActionPerformed
	
	/**
	 * 
	 * @param evt
	 */
	private void jMenuItemAllowancePDFreport(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jMenuItem7ActionPerformed

		// #if ApplicationLogging
//@		 ApplicationLogger.logger.info("Preparing PDF export for Employee"+
//@		 "Allowance.");
		// #endif

		// TODO add your handling code here:
		JFileChooser dialog = new JFileChooser();
		dialog.setSelectedFile(new File("Employee Allowance Report.pdf"));
		int dialogResult = dialog.showSaveDialog(null);
		if (dialogResult == JFileChooser.APPROVE_OPTION) {
			String filePath = dialog.getSelectedFile().getPath();

			try {
				// #if ApplicationLogging
//@				 ApplicationLogger.logger.info("Trying to select all data from TABLE"+
//@				 "ALLOWANCE.");
				// #endif

				String sql = "select * from Allowance";

				pst = conn.prepareStatement(sql);
				rs = pst.executeQuery();

				Document myDocument = new Document();
				PdfWriter myWriter = PdfWriter.getInstance(myDocument, new FileOutputStream(filePath));
				PdfPTable table = new PdfPTable(11);
				myDocument.open();

				float[] columnWidths = new float[] { 3, 7, 7, 5, 5, 9, 6, 5, 8, 8, 8 };
				table.setWidths(columnWidths);

				table.setWidthPercentage(100); // set table width to 100%

				myDocument.add(new Paragraph(messages.getString("emp_allowance_list"),
						FontFactory.getFont(FontFactory.TIMES_BOLD, 20, Font.BOLD)));
				myDocument.add(new Paragraph(new Date().toString()));
				myDocument.add(new Paragraph(
						"-------------------------------------------------------------------------------------------"));
				table.addCell(
						new PdfPCell(new Paragraph("ID", FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("overtime"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("medical"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("bonus"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("other"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("employee_id"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("salary"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("rate"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("allowance"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("firstname"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("surname"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD))));

				while (rs.next()) {

					table.addCell(new PdfPCell(new Paragraph(rs.getString(1),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(2),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(3),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(4),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(5),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(6),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(7),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(8),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(9),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(10),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(11),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.PLAIN))));

				}

				myDocument.add(table);
				myDocument.add(new Paragraph(
						"--------------------------------------------------------------------------------------------"));
				myDocument.close();
				// #if ApplicationLogging
//@				 ApplicationLogger.logger.info("Document successfully created.");
				// #endif

				JOptionPane.showMessageDialog(null, messages.getString("report_sucessful"));

			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, e);

			} finally {

				try {
					rs.close();
					pst.close();

				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, e);

				}
			}
		}

	}// GEN-LAST:event_jMenuItem7ActionPerformed

	private void initComponents() {
		// TODO Auto-generated method stub

	}
	public PDFGenerator() {
		initComponents();
		
	}
}
